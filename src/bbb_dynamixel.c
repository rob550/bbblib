#define EXTERN extern

#include "../include/bbb.h"
#include <sys/time.h>

#define MAP_SIZE 4096UL
#define MAP_MASK (MAP_SIZE - 1)

int Dynam_Init(DynamSetup *dynam){
    
    /* Initialize GPIO */
    bbb_init();
    bbb_initGPIO(61,gpio_output);

    /* Initialize UART Port */
    dynam->fuart = initUART();

    /* GPIO Port Address */
    dynam->port = 0x20000000;

    /* GPIO 1 Command Low and High*/
    dynam->low  = 0x4804c190;
    dynam->high = 0x4804c194;
   
    /* Open File */
    if((dynam->fgpio = open("/dev/mem", O_RDWR | O_SYNC)) == -1) return EXIT_FAILURE;
    
    /* Map Low */
    dynam->map_base_low = mmap(0, MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, 
                                     dynam->fgpio, dynam->low & ~MAP_MASK);
    if(dynam->map_base_low == (void *) -1) return EXIT_FAILURE;
    dynam->virt_addr_low = dynam->map_base_low + (dynam->low & MAP_MASK);
    
    /* Map High */
    dynam->map_base_high = mmap(0, MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED,
                                     dynam->fgpio, dynam->high & ~MAP_MASK);
    if(dynam->map_base_high == (void *) -1) return EXIT_FAILURE;
    dynam->virt_addr_high = dynam->map_base_high + (dynam->high & MAP_MASK);
 
    return EXIT_SUCCESS;
}

int Dynam_Comm(DynamSetup *dynam, DynamPacket *packet){
    
    int i = 0;
    unsigned char data[MAX_PACKET_SIZE];
    unsigned char checksum;
    struct timeval start, now;

    /* Prepare data to send */
    data[0] = 0xFF; /* Dinamixel Header */
    data[1] = 0xFF; /* Dinamixel Header */
    data[2] = packet->id;   /* Servo ID*/
    checksum = packet->id;   /* Initialize Checksum */
    data[3] = (packet->send_size + 2); /* Package Lenght */
    checksum += data[3];
    data[4] = packet->instruction;
    checksum += data[4];
    for(i = 0; i < packet->send_size; i++){
        data[i+5] = packet->send[i];
        checksum += data[i+5];
    }
    checksum = ~(checksum);
    data[i+5] = checksum;

    /* Command Low */
    *((unsigned long *) dynam->virt_addr_low) = dynam->port;

    /* Write data */
    write(dynam->fuart, data, packet->send_size + 6); 
    tcdrain(dynam->fuart);

    /* Command High */
    *((unsigned long *) dynam->virt_addr_high) = dynam->port;

    /* Read data start */
    gettimeofday(&start,NULL);
    checksum = 0;

    /* Read Header Bytes */
    for(i = 0; i < 2; i++){
        
        /* Wait for First Data with Timeout*/
        while(read(dynam->fuart, &(data[i]), 1) < 1){
            gettimeofday(&now,NULL);
            if(now.tv_sec - start.tv_sec + 1e-6*(now.tv_usec - start.tv_usec)
                > READ_TIMEOUT){
                printf("ERROR - Timeout of Read Serial");
                return EXIT_SUCCESS;
            }
        }
 
        /* Check correct header byte */
        if (data[i] != 0xFF){
            i--;
        }
        
    }

    /* Read ID */
    while(read(dynam->fuart, &(data[2]), 1) < 1);
    if(data[2] != packet->id){
        printf("ERROR - Received ID Byte Wrong");
        return EXIT_FAILURE;
    }
    checksum = packet->id;   
 
    /* Read Length */
    while(read(dynam->fuart, &(packet->rcvd_size), 1) < 1);
    checksum += packet->rcvd_size;   
    packet->rcvd_size -= 2; 

    /* Read Error */
    while(read(dynam->fuart, &(packet->error), 1) < 1);
    if(packet->error != 0){
        printf("ERROR - Servo Status Returned Error");
        return EXIT_FAILURE;
    }
    checksum += packet->error;   
  
    /* Read Parameters */
    for(i = 0; i < packet->rcvd_size; i++){
        while(read(dynam->fuart, &(packet->rcvd[i]), 1) < 1);
        checksum += packet->rcvd[i];
    }

    /* Read Checksum */
    while(read(dynam->fuart, &(data[0]), 1) < 1);
    checksum = ~(checksum);
    if(data[0] != checksum){
        printf("ERROR - Received Checksum Error");
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}

int Dynam_Deinit(DynamSetup *dynam){

    /* Deinitialize GPIO */
    bbb_deinitGPIO(61);

    /* Close UART */
    closeUART(dynam->fuart);

    /* Unmap both low and high */
    if(munmap(dynam->map_base_low, MAP_SIZE) == -1) return EXIT_FAILURE;
    if(munmap(dynam->map_base_high, MAP_SIZE) == -1) return EXIT_FAILURE;

    /* Close GPIO Map File */
    close(dynam->fgpio);

    return EXIT_SUCCESS;

}

int Dynam_ReadMode(DynamSetup *dynam, DynamStatus *servo){

    DynamPacket packet;
    
    Dynam_SetModeReadPacket(&packet, servo);
    if(Dynam_Comm(dynam, &packet)) return EXIT_FAILURE;
    Dynam_ReadModePacket(&packet, servo);
    
    return EXIT_SUCCESS;

}

int Dynam_ReadStatus(DynamSetup *dynam, DynamStatus *servo){

    DynamPacket packet;
    
    Dynam_SetReadStatusPacket(&packet, servo);
    if(Dynam_Comm(dynam, &packet)) return EXIT_FAILURE;
    Dynam_ReadStatusPacket(&packet, servo);
    
    return EXIT_SUCCESS;

}

int Dynam_SetModeReadPacket(DynamPacket *packet, DynamStatus *servo){

    packet->id = servo->id;
    packet->instruction = 0x02;
    packet->send[0] = 0x06;
    packet->send[1] = 0x04;
    packet->send_size = 2;
    
    return EXIT_SUCCESS;
}

int Dynam_SetReadStatusPacket(DynamPacket *packet, DynamStatus *servo){

    packet->id = servo->id;
    packet->instruction = 0x02;
    packet->send[0] = 0x24;
    packet->send[1] = 0x08;
    packet->send_size = 2;
    
    return EXIT_SUCCESS;
}

int Dynam_ReadStatusPacket(DynamPacket *packet, DynamStatus *servo){

    unsigned int aux;
    
    if(packet->send[0] != 0x24){
        printf("ERROR - Attempting to Read Wrong Packet\n");
        return EXIT_FAILURE;
    }
    
    /* Read Current Angle */
    aux = (packet->rcvd[1] << 8) | packet->rcvd[0]; 
    servo->cur_angle = ((double) aux)*0.29;

    /* Read Current Speed */
    aux = (packet->rcvd[3] << 8) | packet->rcvd[2]; 
    servo->cur_angle = ((double) aux) / 1023.0;

    /* Read Current Load */
    aux = (packet->rcvd[5] << 8) | packet->rcvd[4]; 
    servo->cur_angle = ((double) aux) / 1023.0;

    /* Read Voltage */
    servo->cur_volt = packet->rcvd[6] / 10.0;

    /* Read Voltage */
    servo->cur_temp = packet->rcvd[7];

    return EXIT_SUCCESS;

}

int Dynam_ReadModePacket(DynamPacket *packet, DynamStatus *servo){

    if(packet->send[0] != 0x06){
        printf("ERROR - Attempting to Read Wrong Packet\n");
        return EXIT_FAILURE;
    }
    
    unsigned int aux;
    float min_angle, max_angle;

    servo->cur_mode = WHEEL;
    aux = (packet->rcvd[1] << 8) | packet->rcvd[0]; 
    if(aux) servo->cur_mode = JOINT;
    min_angle = aux * 0.29;
    aux = (packet->rcvd[3] << 8) | packet->rcvd[2]; 
    max_angle = aux * 0.29;
    if(aux) servo->cur_mode = JOINT;

    return EXIT_SUCCESS; 
}

int Dynam_SetCmdPacket(DynamPacket *packet, DynamStatus *cmd){

    packet->id = cmd->id;
    packet->instruction = 0x03;
    unsigned int aux = 0;
   
    /* Joint Mode */
    if(cmd->cmd_mode == JOINT){    
        packet->send[0] = 0x1E;

        /* Desired Position */
        aux = (unsigned int) (cmd->cmd_angle / 0.29);
        packet->send[1] = aux & 0xFF;
        packet->send[2] = (aux >> 8) & 0xFF;

        /* Desired Speed */
        if(cmd->cmd_speed > 1.0) cmd->cmd_speed = 1.0;
        if(cmd->cmd_speed < 0.0) cmd->cmd_speed = 0.0;
        aux = (unsigned int) (cmd->cmd_speed * 1023); 
        packet->send[3] = aux & 0xFF;
        packet->send[4] = (aux >> 8) & 0xFF;

        /* Desired Torque */
        if(cmd->cmd_torque > 1.0) cmd->cmd_torque = 1.0;
        if(cmd->cmd_torque < 0.0) cmd->cmd_torque = 0.0;
        aux = (unsigned int) (cmd->cmd_torque * 1023); 
        packet->send[5] = aux & 0xFF;
        packet->send[6] = (aux >> 8) & 0xFF;

        packet->send_size = 7;
    }
    /* Wheel Mode */
    else if (cmd->cmd_mode == WHEEL){
        packet->send[0] = 0x20;

        /* Desired Speed */
        if(cmd->cmd_speed > 1.0) cmd->cmd_speed = 1.0;
        if(cmd->cmd_speed < 0.0) cmd->cmd_speed = 0.0;
        aux = (unsigned int) (cmd->cmd_speed * 1023); 
        packet->send[1] = aux & 0xFF;
        packet->send[2] = (aux >> 8) & 0xFF;

        /* Desired Torque */
        if(cmd->cmd_torque > 1.0) cmd->cmd_torque = 1.0;
        if(cmd->cmd_torque < 0.0) cmd->cmd_torque = 0.0;
        aux = (unsigned int) (cmd->cmd_torque * 1023); 
        packet->send[3] = aux & 0xFF;
        packet->send[4] = (aux >> 8) & 0xFF;

        packet->send_size = 5;
    }
    else
        return EXIT_FAILURE;
   
    return EXIT_SUCCESS;
}


int Dynam_PrintPacket(DynamPacket *packet){

    int i = 0;

    printf("ID:           0x%02x\n", packet->id);
    printf("Instruction:  0x%02x\n", packet->instruction);

    for(i = 0; i < packet->send_size; i++)
        printf("Send %d Byte:  0x%02x\n", i, packet->send[i]);
    
    printf("Error:  0x%02x\n", packet->error);
 
    for(i = 0; i < packet->rcvd_size; i++)
        printf("Rcvd %d Byte:  0x%02x\n", i, packet->rcvd[i]);
   
    return EXIT_SUCCESS;
 
}

int Dynam_PrintStatus(DynamStatus *status){

    printf("\t Status For Servo ID #%d\n\n",status->id);
    if(status->cmd_mode)   printf("Command Mode: \t WHEEL \n");
    else           printf("Command Mode: \t JOINT \n");
    if(status->cur_mode)   printf("Current Mode: \t WHEEL \n\n");
    else           printf("Current Mode: \t JOINT \n\n");

    printf(" COMMANDED Values:\n");
    if(!(status->cmd_mode))   printf(" Angle:       %.1lf\n",status->cmd_angle);
    printf(" Speed:       %.1lf\n",status->cmd_speed);
    printf(" Torque:      %.1lf\n\n",status->cmd_torque);

    printf(" CURRENT Values:\n");
    printf(" Angle:       %.1lf\n",status->cur_angle);
    printf(" Speed:       %.1lf\n",status->cur_speed);
    printf(" Load:        %.1lf\n",status->cur_load);
    printf(" Voltage:     %.1lf\n",status->cur_volt);
    printf(" Temperatue:  %.1lf\n\n",status->cur_temp);

    return EXIT_SUCCESS;
} 
