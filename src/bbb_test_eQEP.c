 /*
  *  Beaglebone Black Hardware Interface Function Library
  *  For Use in Robotics 550, University of Michigan
  *
  *  References:  
  *      http://beagleboard.org/
  *      BlackLib: http://free.com/projects/blacklib
  *
  *  bbb_test_eQEP.c:  Test eQEP
  *
  */

#define EXTERN  // Needed for global data declarations in bbb.h
#include "bbb.h"


int main()
{
  byte encA_pin=68; // GPIO_68
  byte encB_pin=69; //GPIO_69
  int i;


  // Initialize eQEP2
  if(bbb_initEQEP(2)){
    printf("Error initializing eQEP\n");
    return -1;
  }

  // Initialize BBB and two GPIO pins

  if (bbb_init()) {
    printf("Error initializing BBB.\n");
    return -1;
  }

  if (bbb_initGPIO(encA_pin,gpio_output)) {
    printf("Error initializing BBB GPIO pin %d.\n", (int) encA_pin);
    return -1;
  }

  if (bbb_initGPIO(encB_pin,gpio_output)) {
    printf("Error initializing BBB GPIO pin %d.\n", (int) encB_pin);
    return -1;
  }

  // Move simulated encoder forward 100 ticks and back 100 ticks

  bbb_writeGPIO(encA_pin, 0);
  bbb_writeGPIO(encB_pin, 0);
  bbb_setPositionEQEP(2, 0);
  printf("count:%d\n", bbb_getPositionEQEP(2));

  for (i=0;i<50;i++) {
    bbb_toggleGPIO(encA_pin);
    usleep(100);
    printf("count:%d\n", bbb_getPositionEQEP(2));
    bbb_toggleGPIO(encB_pin);
    printf("count:%d\n", bbb_getPositionEQEP(2));
    usleep(100);
  }

  for (i=0;i<50;i++) {
    bbb_toggleGPIO(encB_pin);
    printf("count:%d\n", bbb_getPositionEQEP(2));
    usleep(100);
    bbb_toggleGPIO(encA_pin);
    printf("count:%d\n", bbb_getPositionEQEP(2));
    usleep(100);
  }

  // Deinitialize GPIO
  bbb_deinitGPIO(encA_pin);
  bbb_deinitGPIO(encB_pin);


  return 0;
}
