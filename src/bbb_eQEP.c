/*
  *  Beaglebone Black Hardware Interface Function Library
  *  For Use in Robotics 550, University of Michigan
  *
  * bbb_initEQEP()
  * bbb_setPositionEQEP()
  * bbb_setPeriodEQEP()
  * bbb_setModeEQEP()
  * bbb_getPositionEQEP()
  * bbb_getPeriodEQEP()
  * bbb_getModeEQEP()
  *
  */



#define EXTERN extern
#include "bbb.h"


#define eQEP0 "/sys/devices/ocp.3/48300000.epwmss/48300180.eqep"
#define eQEP1 "/sys/devices/ocp.3/48302000.epwmss/48302180.eqep"
#define eQEP2 "/sys/devices/ocp.3/48304000.epwmss/48304180.eqep"

#define EQEP_MODE_REL 1
#define EQEP_MODE_ABS 0

int bbb_initEQEP(int eQEPnum){
	if((eQEPnum > 2) | (eQEPnum < 0)){
		printf("ERROR: Invalid eQEP");
		return -1;
	}
	sprintf(eqepDir[0],"%s", eQEP0);
	sprintf(eqepDir[1],"%s", eQEP1);
	sprintf(eqepDir[2],"%s", eQEP2);


	bbb_setPositionEQEP(eQEPnum, 0);
	return 0;
}

int bbb_setPositionEQEP(int eQEPnum, int position){
	FILE *fp;
    char fname[60];
    sprintf(fname,"%s/position", eqepDir[eQEPnum]);
	fp = fopen(fname, "w");
	fprintf(fp, "%d\n", position);
	fclose(fp);
	return 0;
}

int bbb_setPeriodEQEP(int eQEPnum, uint64_t period){
	FILE *fp;
    char fname[60];
    sprintf(fname,"%s/period", eqepDir[eQEPnum]);
    fp = fopen(fname, "w");
    fprintf(fp, "%"PRId64"\n", period);
    fclose(fp);
    return 0;
}

int bbb_setModeEQEP(int eQEPnum, int mode){
	FILE *fp;
    char fname[60];
    sprintf(fname,"%s/mode", eqepDir[eQEPnum]);
    fp = fopen(fname, "w");
    
    if(mode == EQEP_MODE_REL){
		fprintf(fp, "%d\n", EQEP_MODE_REL);
	}
    else if(mode == EQEP_MODE_ABS) {
    	fprintf(fp, "%d\n", EQEP_MODE_ABS);
    }
    else {
    	printf("ERROR: Invalid Mode\n");
    	fclose(fp);
    	return -1;
    }
    fclose(fp);
    return 0;
}

int bbb_getPositionEQEP(int eQEPnum){
	int position;
	FILE *fp;
    char fname[60];
    sprintf(fname,"%s/position", eqepDir[eQEPnum]);
    fp = fopen(fname, "r");
    fscanf(fp, "%d", &position);
    fclose(fp);
    return position;
}

uint64_t bbb_getPeriodEQEP(int eQEPnum){
	uint64_t period;
	FILE *fp;
    char fname[60];
    sprintf(fname,"%s/period", eqepDir[eQEPnum]);
    fp = fopen(fname, "r");
    fscanf(fp, "%"PRId64"", &period);
    fclose(fp);
    return period;
}

int bbb_getModeEQEP(int eQEPnum){
	int mode;
	FILE *fp;
    char fname[60];
    sprintf(fname,"%s/mode", eqepDir[eQEPnum]);
    fp = fopen(fname, "r");
    fscanf(fp, "%d", &mode);
    fclose(fp);
    return mode;
}