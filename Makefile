TEST_CFILES := $(wildcard src/*test*.c)
C_FILES := $(filter-out $(TEST_CFILES), $(wildcard src/*.c))
OBJ_FILES := $(addprefix obj/,$(notdir $(C_FILES:.c=.o)))
DEPS := $(wildcard include/*.h)

IFLAGS := -I./include 
CCFLAGS := -g -Wall $(IFLAGS)
LDFLAGS := -lm -lpthread

EXE_FILES := $(addprefix bin/,$(notdir $(TEST_CFILES:.c=)))

CC = gcc

.PRECIOUS: $(OBJ_FILES)

all: $(EXE_FILES)

obj: $(OBJ_FILES)

bin/%: src/%.c $(OBJ_FILES)
	$(CC) $(CCFLAGS) -o $@ $^ $(LDFLAGS)

obj/%.o: src/%.c $(DEPS)
	$(CC) $(CCFLAGS) -c -o $@ $<

clean:
	rm -f bin/* obj/*
