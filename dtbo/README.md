Place the dtbo files in this folder into /lib/firmware

$> sudo cp *.dtbo /lib/firmware

edit the /etc/rc.local file and 

$> sudo nano /etc/rc.local


add the following lines to initialize all the relevant device overlays on startup

#setup beaglebone for ROB550 Breakout Cape

echo BB-SPI0-01 > /sys/devices/bone_capemgr.9/slots     #SPI0
echo BB-ADC > /sys/devices/bone_capemgr.9/slots         #ADC
echo am33xx_pwm > /sys/devices/bone_capemgr.9/slots     #PWMS
echo bone_pwm_P9_14 > /sys/devices/bone_capemgr.9/slots #SV1
echo bone_pwm_P9_16 > /sys/devices/bone_capemgr.9/slots #SV2
echo bone_pwm_P8_19 > /sys/devices/bone_capemgr.9/slots #SV3
echo bone_pwm_P8_13 > /sys/devices/bone_capemgr.9/slots #SV4
echo bone_pwm_P9_42 > /sys/devices/bone_capemgr.9/slots #SV5
echo BB-UART4 > /sys/devices/bone_capemgr.9/slots #UART4
#echo bone_eqep0 > /sys/devices/bone_capemgr.9/slots #eQEP 0 not broken out
#echo bone_eqep1 > /sys/devices/bone_capemgr.9/slots #eQEP 1 on PRUIO port
#echo bone_eqep2 > /sys/devices/bone_capemgr.9/slots #eQEP 2 conflict with HDMI/ not broken out
echo bone_eqep2b > /sys/devices/bone_capemgr.9/slots #eQEP 2 on GPIO 44, 45, 46, 47

#END

reboot the beaglebone for the changes to take effect 